package compteur

/**
 * La classe des compteurs paramétrés par une liste non vide de valeurs maximale pour chaque roue.
 *  La liste ne peut être vide et les valeurs sont forcément positives ou nulles
 */
class CompteurImpl extends Compteur {

  var tab: Int 
  
  def init(l: List[Int]) = {

    for (i <- l) {
      tab::List(i)
 

    }

  }

  def courant: Int = {
   tab
  }
  def suivant = {
   
  }
  def suivantPossible : =  {}
  def valPossibles = ???
  def valRestantes = ???
}